﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using Rma.Exercise.Repository.Sql.Configuration;

namespace Rma.Exercise.Repository.Sql
{
    public abstract class BaseRepository<T>
    {
        private readonly ISqlConfiguration _sqlConfiguration;

        protected BaseRepository(ISqlConfiguration sqlConfiguration)
        {
            _sqlConfiguration = sqlConfiguration;
        }
        protected IDbConnection Connection => new SqlConnection(_sqlConfiguration.DatabaseConnectionString);

    }
}