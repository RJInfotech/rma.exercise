﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Rma.Exercise.Repository.Sql.Configuration;
using Rma.Exercise.Shared.Model;

namespace Rma.Exercise.Repository.Sql
{
    public class TradeRepository : BaseRepository<Trade>, ITradeRepository
    {
        private const string InsertTradeStoredProcedure = "dbo.insert_trade";
        public TradeRepository(ISqlConfiguration sqlConfiguration) : base(sqlConfiguration)
        {
        }


        public async Task<int> InsertBulkAsync(IEnumerable<Trade> trades)
        {
            using (var connection = Connection)
            {
                connection.Open();
                var affectedRows = await connection.ExecuteAsync(InsertTradeStoredProcedure,
                    trades.Select(t => new
                    {
                        Source = t.Source.ToString(),
                        Destination = t.Destination.ToString(),
                        t.Lots,
                        t.Price
                    }),
                    commandType: CommandType.StoredProcedure);
                return affectedRows;
            }
        }


    }
}
