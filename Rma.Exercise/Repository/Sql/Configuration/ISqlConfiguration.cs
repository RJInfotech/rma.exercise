﻿namespace Rma.Exercise.Repository.Sql.Configuration
{
    public interface ISqlConfiguration
    {
        string DatabaseConnectionString { get; set; }
    }
}
