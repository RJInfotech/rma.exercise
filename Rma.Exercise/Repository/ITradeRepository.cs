﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Rma.Exercise.Shared.Model;

namespace Rma.Exercise.Repository
{
    public interface ITradeRepository
    {
        Task<int> InsertBulkAsync(IEnumerable<Trade> trades);
    }
}