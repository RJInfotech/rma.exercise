﻿using System.IO;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace Rma.Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            // Using dependency injection, register types
            // ITradeRepository -> TradeRepository
            // ITradeService -> TradeService
            // ISqlConfiguration -> AppConfiguration.

            // Introduced Serilogs

            var configuration = GetConfiguration();

            ILogger logger = new LoggerConfiguration()
            .ReadFrom.Configuration(configuration)
            .CreateLogger();

            // Register in Dependency injection

            // Resolve ITradeService
            // and call methods
        }


        private static IConfigurationRoot GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            return builder.Build();
        }
    }
}
