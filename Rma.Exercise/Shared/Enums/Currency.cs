﻿namespace Rma.Exercise.Shared.Enums
{
    public enum Currency
    {
        Aud,
        Usd,
        Eur,
        Jpy,
        Sgd,
        Gbp
    }
}
