﻿using System;
using System.Threading;
using Rma.Exercise.Shared.Enums;

namespace Rma.Exercise.Shared.Model
{

    public class Trade
    {
        private const decimal LotSize = 1000000M;
        private string _currencyPair;
        private Currency _source;
        private Currency _destination;

        public string CurrencyPair
        {
            get
            {
                return _currencyPair;
            }
            set
            {

                _source = GetCurrency(value.Substring(0, 3));
                _destination = GetCurrency(value.Substring(3, 6));
                _currencyPair = value;
            }
        }

        public Currency Source => _source;
        public Currency Destination => _destination;

        public int Amount { get; set; }
        public decimal Lots => Amount / LotSize;
        public decimal Price { get; set; }

        private Currency GetCurrency(string val)
        {
            var enumValue = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(val);
            return (Currency)Enum.Parse(typeof(Currency), enumValue);
        }
    }
}
