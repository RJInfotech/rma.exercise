﻿using Rma.Exercise.Repository.Sql.Configuration;

namespace Rma.Exercise
{
    public class AppConfiguration : ISqlConfiguration
    {
        public string DatabaseConnectionString { get; set; }

        public AppConfiguration()
        {
            var configuration = GetConfiguration();
            DatabaseConnectionString = configuration["Sql.Database.ConnectionString"];
        }

        
    }
}
