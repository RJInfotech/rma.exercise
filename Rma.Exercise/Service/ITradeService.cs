﻿using System.IO;
using System.Threading.Tasks;

namespace Rma.Exercise.Service
{
    public interface ITradeService
    {
        Task CreateTradesFromStreamAsync(Stream stream);
    }
}