﻿using System.IO;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using Rma.Exercise.Repository;
using Rma.Exercise.Service.Helper;
using Rma.Exercise.Service.Map;
using Rma.Exercise.Shared.Model;
using Serilog;

namespace Rma.Exercise.Service
{
    public class TradeService : ITradeService
    {
        private readonly ILogger _logger;
        private readonly ITradeRepository _tradeRepository;
        private readonly ICsvHelper _csvHelper;

        public TradeService(ILogger logger, ITradeRepository tradeRepository, ICsvHelper csvHelper)
        {
            _logger = logger;
            _tradeRepository = tradeRepository;
            _csvHelper = csvHelper;
        }

        public async Task CreateTradesFromStreamAsync(Stream stream)
        {
            // TODO: Introduce more loggings.
            var csv = _csvHelper.GetCsvReaderFromStream(stream);
            var trades = csv.GetRecords<Trade>();
            await _tradeRepository.InsertBulkAsync(trades);
        }

    }
}
