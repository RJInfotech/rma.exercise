﻿using System;
using System.Threading;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using Rma.Exercise.Shared.Enums;
using Rma.Exercise.Shared.Model;

namespace Rma.Exercise.Service.Map
{
    public class TradeMap : ClassMap<Trade>
    {
        public TradeMap()
        {
            Map(m => m.CurrencyPair).Index(0)
                .Validate(field =>
                {
                    if (string.IsNullOrEmpty(field))
                    {
                        return false;
                    }
                    
                    if (field.Length != 6)
                    {
                        return false;
                    }

                    var currentCulture = Thread.CurrentThread.CurrentCulture;
                    var source = currentCulture.TextInfo.ToTitleCase(field.Substring(0, 3));
                    var destination = currentCulture.TextInfo.ToTitleCase(field.Substring(4, 6));

                    return Enum.IsDefined(typeof(Currency), source) &&
                           Enum.IsDefined(typeof(Currency), destination);
                });

            Map(m => m.Amount).Index(1)
                .TypeConverter<Int32Converter>();

            Map(m => m.Price).Index(2)
                .TypeConverter<DecimalConverter>();
        }
    }
}
