﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;
using Rma.Exercise.Service.Map;
using Serilog;

namespace Rma.Exercise.Service.Helper
{
    public class CsvHelper : ICsvHelper
    {
        private readonly ILogger _logger;

        public CsvHelper(ILogger logger)
        {
            _logger = logger;

            // TODO: Can introduce CSV configuration from appsettings.json through ICsvConfiguration.
        }

        public IReader GetCsvReaderFromStream(Stream stream)
        {

            // TODO: Use the configuration from ICsvConfiguration, wherever applicable.

            var csv = new CsvReader(new StreamReader(stream));
            csv.Configuration.RegisterClassMap<TradeMap>();
            csv.Configuration.BadDataFound = context =>
            {
                _logger.Warning($"Bad data found on row '{context.RawRow}'");
            };
            csv.Configuration.ReadingExceptionOccurred = exception =>
            {
                _logger.Warning($"Reading exception: {exception.Message}");
            };
            csv.Configuration.MissingFieldFound = (headerNames, index, context) =>
            {
                _logger.Warning(
                    $"Field with names ['{string.Join("', '", headerNames)}'] at index '{index}' was not found. ");
            };

            csv.Configuration.HasHeaderRecord = false;
            csv.Configuration.DetectColumnCountChanges = true;
            csv.Configuration.ShouldSkipRecord = record => record.Length != 3;
            csv.Configuration.TrimOptions = TrimOptions.Trim | TrimOptions.InsideQuotes;
            return csv;
        }
    }
}
