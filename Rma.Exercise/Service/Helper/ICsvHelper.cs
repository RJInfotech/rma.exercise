﻿using System.IO;
using CsvHelper;

namespace Rma.Exercise.Service.Helper
{
    public interface ICsvHelper
    {
        IReader GetCsvReaderFromStream(Stream stream);
    }
}