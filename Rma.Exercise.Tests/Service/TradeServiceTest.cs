﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Rma.Exercise.Repository;
using Rma.Exercise.Service;
using Serilog;
using Xunit;

namespace Rma.Exercise.Tests.Service
{
    public class TradeServiceTest
    {
        private readonly TradeService _tradeService;
        public TradeServiceTest()
        {
            Mock<ITradeRepository> tradeRepository = new Mock<ITradeRepository>();
            Mock<ILogger> logger = new Mock<ILogger>();
            _tradeService = new TradeService(logger.Object, tradeRepository.Object);
        }

        [Fact]
        public void CreateTradesFromStreamAsync_InvalidStream_ShouldThrowsException()
        {
            Assert.Equal(4, 2+2);
        }

        [Theory]
        [InlineData("FileNameWithNoRecord", 0)]
        [InlineData("FileNameWithOneInValidNumberOfColumns", 0)]
        [InlineData("FileNameWithOneInvalidRecords", 0)] // CurrencyPair (<6 characters, empty, invalid enums), amount (invalid int, null), price (invalid decimal, null) 
        [InlineData("FileNameWithValidRecords", 5)]
        [InlineData("FileNameWithMixedRecords", 5)]
        public void CreateTradesFromStreamAsync_ValidStream_ShouldCreateRecordsInRepository(string fileName, int validRecords)
        {
            // Read the file
            // call the service
            // assert the records passed to the mock repository;
        }
    }
}
